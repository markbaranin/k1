/**
   Testing in browser via COMWebsocketBridge
   var ws = new WebSocket("ws://127.0.0.1:8181");
   ws.send('{"action":"setChannel","channel":1,"val":255}');
   ws.send('{"action":"setTimeofDay","val":"day"}');
   ws.send('{"action":"setTimeofDay","val":"night"}');

   Testing via serial
   {"action":"setChannel","channel":1}
   {"action":"setTimeofDay","val":"day"}
   {"action":"setTimeofDay","val":"night"}
   {"action":"loftSelected","number":10}
   {"action":"startScreensaver"}
*/

#include <DmxSimple.h>
#include <ArduinoJson.h>

const long heartbeatInterval = 10 * 1000;
unsigned long lastHeartbeatSent = 0;

const int TOTAL_DMX_CHANNELS = 96;
const int MAX_BRIGHTNESS = 255;

const int NUM_LEDS = 8;
// first one is dummy
int LEDS[NUM_LEDS + 1] = {
  0, //  dummy
  1,
  2,
  3,
  4,
  5,
  6,
  7

};

const int NUM_STAGE5 = 3;
int STAGE5[NUM_STAGE5] = {86, 89, 90};

void setup()
{
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
  DmxSimple.usePin(4);
  DmxSimple.maxChannel(TOTAL_DMX_CHANNELS);


  Serial.begin(9600);
  Serial.setTimeout(125);
  delay(50);
  sendAction("boot");
  Blackout();
}

void readSerial();

void loop()
{
  readSerial();
  delay(5);
}


void Blackout() {
  for (int i = 1; i <= TOTAL_DMX_CHANNELS; i++) {
    DmxSimple.write(i, 0);
  }
}


const int fromSerialQueueLength = 5;
String fromSerialQueue[fromSerialQueueLength];
void readSerial()
{
  String fromSerial = "";
  char character;

  if (Serial.available() > 0)
  {
    for (int q = 0; q < fromSerialQueueLength; q++)
    {
      fromSerialQueue[q] = "";
    }

    int i = 0;
    while (Serial.available() > 0)
    {
      //set led lopp laks kaduma
      fromSerial = Serial.readStringUntil('}');
      if (fromSerial == "\n") {
        continue;
      }

      fromSerial.concat("}");
      fromSerialQueue[i] = fromSerial;
      i++;
      if (i >= fromSerialQueueLength)
      {
        break;
      }
      delay(1);
    }
    for (int j = 0; j < i; j++)
    {
      //Serial.println(String(j) + " : " + fromSerialQueue[j]);
      String fromQueue = fromSerialQueue[j];
      fromSerialQueue[j] = "";

      if (fromQueue.length() >= 2)
      {
        StaticJsonDocument<200> doc;
        DeserializationError error = deserializeJson(doc, fromQueue);
        if (error)
        {
          sendAction("invalidMessageReceived");
          /*
            Serial.println(fromSerial);
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
          */
          continue;
        }
        else
        {
          JsonObject root = doc.as<JsonObject>();
          if (root.containsKey("action"))
          {
            String action = root["action"].as<String>();
            if (action == "setChannel")
            {
              int channel = root["channel"].as<int>();
              if (0 < channel && channel <= TOTAL_DMX_CHANNELS)
              {
                Blackout();
                DmxSimple.write(channel, MAX_BRIGHTNESS);
              }
            }
            else if (action == "LedSelected")
            {
              int number = root["number"].as<int>();
              Blackout();
              DmxSimple.write(LEDS[number], MAX_BRIGHTNESS);
            }
            else if (action == "startScreensaver")
            {
              Blackout();
            }
          }
        }
      }
    }
  }
}

void sendJson(StaticJsonDocument<256> doc)
{
  serializeJson(doc, Serial);
  Serial.print("\n");
  delay(5);
}

void sendAction(String actionValue)
{
  sendAction(actionValue, NULL);
}

void sendAction(String actionValue, int value)
{
  StaticJsonDocument<256> doc;
  doc["action"] = actionValue;
  if (value != NULL)
  {
    doc["value"] = value;
  }
  sendJson(doc);
}

void sendAction(String actionValue, String value)
{
  //has to be local. otherwise will have to clear a buffer or there will be a memory leak
  StaticJsonDocument<256> doc;
  doc["action"] = actionValue;
  if (value != NULL)
  {
    doc["value"] = value;
  }
  sendJson(doc);
}

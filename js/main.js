let lang;

var object_info = [
	{ "button_name": "Btn_RMembraan", "pointer_joon": "RMembraan_Pointer", "translate_text": "Btn_RMembraan_text" },
	{ "button_name": "Btn_TMembraan", "pointer_joon": "TMembraan_Pointer", "translate_text": "Btn_TMembraan_text" },
	{ "button_name": "Btn_Rakutuum", "pointer_joon": "Rakutuum_Pointer", "translate_text": "Btn_Rakutuum_text", "LED_number": "1" },
	{ "button_name": "Btn_Tuumake", "pointer_joon": "Tuumake_Pointer", "translate_text": "Btn_Tuumake_text", "LED_number": "2" },
	{ "button_name": "Btn_Tsütoplasma", "pointer_joon": "TsytoP_Pointer", "translate_text": "Btn_Tsytoplasma_text" },
	{ "button_name": "Btn_Tsütoskelett", "pointer_joon": "TsytoS_Pointer", "translate_text": "Btn_Tsytoskelett_text" },
	{ "button_name": "Btn_Lysosoom", "pointer_joon": "Lysosoom_Pointer", "translate_text": "Btn_Lysosoom_text", "LED_number": "3" },
	{ "button_name": "Btn_SileEndo", "pointer_joon": "SileEndo_Pointer", "translate_text": "Btn_SileEndo_text", "LED_number": "4" },
	{ "button_name": "Btn_Golg", "pointer_joon": "Golg_Pointer", "translate_text": "Btn_Golg_text", "LED_number": "5" },
	{ "button_name": "Btn_Mitokonder", "pointer_joon": "Mito_Pointer", "translate_text": "Btn_Mitokonder_text", "LED_number": "6" },
	{ "button_name": "Btn_KareEndo", "pointer_joon": "KareEndo_Pointer", "translate_text": "Btn_KareEndo_text", "LED_number": "7" },
	{ "button_name": "Btn_Ribosoom", "pointer_joon": "Ribo_Pointer", "translate_text": "Btn_Ribosoom_text" }
];

$(document).ready(function () {
	start();
	lang = 'et';

	getTranslations('et');

	jQuery('.languages div').click(function () {
		lang = jQuery(this).attr('data-language');
		if (lang) {
			getTranslations(lang);
		}
	});

	$('.btn').click(function () {
		var button = $(this).attr('id');

		var object_text = object_info.find(element => element.button_name === button);

		resetButtons();
		resetPointers();
		if ($(this).find('img.lamp').length !== 0) {
			var lamp_number = object_text.LED_number
			sendActionLightup("setChannel", lamp_number)
		}
		else {
			sendAction("startScreensaver")
		}

		$(this).css({ "color": "#f1f1ec;" });
		$(this).css({ "background": "#cc0066" });
		$(this).css({ "border-color": "#cc0066" });
		$(this).find("img").css({ "visibility": "visible" });
		jQuery(".info_text > p").attr("data-translation", button + "_text");
		getTranslations(lang);

	});
});

function onMessage(evt) {
	try {

	} catch (Ex) {
		console.log(Ex);
	}
}
function goToView(viewNumber) {
	//clearTimer();
	if (viewNumber != 1) {

	}
	$(".view").removeClass("show");
	$(".view:nth-of-type(" + viewNumber + ")").addClass("show");
}

function resetButtons() {
	$('.btn').css({ "background": "#33334d", "color": "white", "border-color": "white" });
}

function resetPointers() {
	$('.pointer_right').css({ "visibility": "hidden" });
	$('.pointer_left').css({ "visibility": "hidden" });
	$('.lamp_left').css({ "visibility": "hidden" });
	$('.lamp_right').css({ "visibility": "hidden" });
}
//TRANSLATIONS
function getTranslations(lang) {
	jQuery('.languages div').removeClass('active');
	jQuery('.languages div[data-language="' + lang + '"]').addClass('active');
	var json = getLangJSON(lang);
	jQuery.each(json, function (key, data) {
		jQuery.each(data, function (key1, data1) {
			jQuery('[data-translation="' + key1 + '"]').html(data1).val(data1);
		});
	});
}

function getLangJSON(lang) {
	var json;
	if (lang == 'et') {
		json = translations.et;
	} else if (lang == 'en') {
		json = translations.en;
	} else if (lang == 'ru') {
		json = translations.ru;
	} else if (lang == 'fi') {
		json = translations.fi;
	}
	return json;
}

function resetText() {
	jQuery(".info_text > p").attr("data-translation", "object_desc");
	getTranslations(lang);
}